.. Basthon documentation master file, created by
   sphinx-quickstart on Sun Dec  3 22:23:51 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Basthon --- Documentation utilisateur
=====================================

.. note::

   Même si nous limitons au maximum la gêne des utilisateurs,
   ce projet est activement développé et l'API peut changer fréquement.



.. toctree::
   :maxdepth: 1
   :caption: Contenu:

   parts/auto-evaluation
   parts/basthon-module
