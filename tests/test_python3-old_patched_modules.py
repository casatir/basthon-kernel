from pathlib import Path
import re
import base64
from utils import same_content


def test_all(selenium_py3old):
    # ensure all patched modules are tested
    tested = set(
        g[len("test_") :]
        for g in globals()
        if g.startswith("test_") and g != "test_all"
    )
    data = selenium_py3old.run_basthon(
        """
    from basthon import _patch_modules
    set(g[len('patch_'):] for g in dir(_patch_modules)
        if g.startswith('patch_'))"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    patched = eval(result["result"]["text/plain"])
    assert tested == patched


def test_time(selenium_py3old):
    import time

    t0 = time.perf_counter()
    result = selenium_py3old.run_basthon("import time ; time.sleep(3)")
    t1 = time.perf_counter()
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert abs(t1 - t0 - 3) < 0.1


def test_PIL(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
    from PIL import Image, ImageDraw
    w, h = 120, 90
    bbox = [(10, 10), (w - 10, h - 10)]

    img = Image.new("RGB", (w, h), "#f9f9f9")
    dctx = ImageDraw.Draw(img)
    dctx.rectangle(bbox, fill="#ddddff", outline="blue")
    img.show()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    img = result["display"]["content"]["image/png"]
    assert same_content(selenium_py3old, "python3-old_pil.png", img)


def test_matplotlib(selenium_py3old):
    selenium_py3old.run_basthon(
        """
    import matplotlib.pyplot as plt

    plt.figure()
    plt.plot([0, 1], [0, 1])
    plt.show()
    """,
        return_data=False,
    )
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium_py3old.run_js(
        "return window._basthon_eval_data.display.content.outerHTML;"
    )
    html = re.sub("matplotlib_[0-9a-f]+", "matplotlib_", html)
    assert same_content(selenium_py3old, f"python3-old_matplotlib.html", html)

    png = selenium_py3old.run_js(
        "return window._basthon_eval_data.display.content.getElementsByTagName('canvas')[0].toDataURL('image/png');"
    )
    png = base64.b64decode(png[len("data:image/png;base64,") :])
    assert same_content(
        selenium_py3old, f"python3-old_matplotlib.png", png, binary=True
    )

    # case where plot and show are called separately
    selenium_py3old.run_basthon(
        """
    plt.figure()
    plt.plot([0, 1], [0, 1])
     """,
        return_data=False,
    )
    selenium_py3old.run_basthon("plt.show()", return_data=False)

    png2 = selenium_py3old.run_js(
        "return window._basthon_eval_data.display.content.getElementsByTagName('canvas')[0].toDataURL('image/png');"
    )
    png2 = base64.b64decode(png2[len("data:image/png;base64,") :])
    assert png2 == png

    # testing animation
    data = selenium_py3old.run_basthon(
        """
import matplotlib.pyplot as plt
import matplotlib.animation

fig = plt.figure()
ax = fig.gca(xlim=[-2, 2], ylim=[0, 4])
curve, = ax.plot([],[], '-b')

def animate(i):
    N = 2 * i + 3
    xx = [4 * k / (N - 1) - 2 for k in range(N)]
    yy = [x ** 2 for x in xx]
    curve.set_data(xx, yy)

ani = matplotlib.animation.FuncAnimation(fig, animate, frames=10, blit=False, interval=200)
ani"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    html = result["result"]["text/html"]
    html = re.sub("_anim_img[0-9a-f]+", "_anim_img", html)
    html = re.sub("_anim_slider[0-9a-f]+", "_anim_slider", html)
    html = re.sub("_anim_radio[1-3]_[0-9a-f]+", "_anim_radio_", html)
    html = re.sub("_anim_loop_select[0-9a-f]+", "_anim_loop_select", html)
    html = re.sub("anim[0-9a-f]+", "anim", html)
    assert same_content(selenium_py3old, "python3-old_matplotlib_animation.html", html)


def test_folium(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
    import folium

    m = folium.Map(location=[47.228382, 2.062796],
                   zoom_start=17)

    m.display()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    map = re.sub("map_[0-9a-f]+", "map_", result["display"]["content"]["text/html"])
    map = re.sub("tile_layer_[0-9a-f]+", "tile_layer_", map)
    assert same_content(selenium_py3old, "python3-old_folium.html", map)


def test_pandas(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
    import pandas as pd

    df = pd.DataFrame({
        'language': ["Python", "C", "Java"],
        'verbosity': [0.1, 0.5, 0.9]
    })

    df.display()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    html = result["display"]["content"]["text/html"]
    assert same_content(selenium_py3old, "python3-old_pandas.html", html)


def test_sympy(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
    import sympy
    from sympy.abc import x
    sympy.pretty_print(sympy.sqrt(x ** 2 + sympy.pi))
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "sympy"
    assert result["display"]["content"] == "$$\\sqrt{x^{2} + \\pi}$$"


def test_turtle(selenium_py3old):
    selenium_py3old.run_basthon(
        """
    import turtle
    turtle.forward(100)
    turtle.done()
    """,
        return_data=False,
    )
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    svg = selenium_py3old.run_js(
        "return window._basthon_eval_data.display.content.outerHTML"
    )
    svg = re.sub('"af_[0-9a-f]+_', '"af_', svg)
    assert same_content(selenium_py3old, "python3-old_turtle.svg", svg)

    # with animation disabled
    selenium_py3old.run_basthon(
        """
    turtle.animation("off")
    turtle.forward(100)
    turtle.done()
    """,
        return_data=False,
    )
    svg = selenium_py3old.run_js(
        "return window._basthon_eval_data.display.content.outerHTML"
    )
    svg = re.sub('"af_[0-9a-f]+_', '"af_', svg)
    assert same_content(selenium_py3old, "python3-old_turtle-anim-off.svg", svg)


def test_scipy(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
    from scipy.integrate import odeint
    import numpy as np

    def f(y, t, a):
        return [-a * y[1], a * y[0]]

    y0 = [1, 0]
    y = odeint(f, y0, [0, np.pi], args=(2,), hmax=1e-2)

    np.allclose(y[-1, :], y0)"""
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["result"]["result"]["text/plain"] == "True"


def test_qrcode(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
    import qrcode

    code = qrcode.make("https://basthon.fr/", format='svg')
    code.show()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    svg = result["display"]["content"]["image/svg+xml"]
    assert same_content(selenium_py3old, "python3-old_qrcode-basthon.svg", svg)


def test_osmiter(selenium_py3old):
    # tested in test_pyroutelib3
    assert True


def test_pyroutelib3(selenium_py3old):
    result = selenium_py3old.run_basthon(
        """
from pyroutelib3 import Router

depart = [47.08428480854615, 2.3939454203269217]
arrivee = [47.08187952227819, 2.398992969262373]

router = Router("foot")
node_depart = router.findNode(*depart)
node_arrivee = router.findNode(*arrivee)
status, route = router.doRoute(node_depart, node_arrivee)
status
"""
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["result"]["result"]["text/plain"] == "'success'"


def test_numpy(selenium_py3old):
    # already tested in test_base and test_scipy
    assert True


def test_micropip(selenium_py3old):
    # at this point, folium should have loaded requests
    # so we check that this is our version
    result = selenium_py3old.run_basthon(
        """
    import requests
    requests.__author__ == 'Romain Casati'"""
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["result"]["result"]["text/plain"] == "True"


def test_binarytree(selenium_py3old):
    from ast import literal_eval

    result = selenium_py3old.run_basthon(
        """
from binarytree import Node

root = Node(1)
root.left = Node(2)
root.right = Node(3)
root.left.left = Node(4)
root.left.right = Node(5)

print(root)
root.graphviz().source
"""
    )
    assert result["stderr"] == ""
    assert (
        result["stdout"]
        == """
    __1
   /   \\
  2     3
 / \\
4   5

"""
    )
    gv = literal_eval(result["result"]["result"]["text/plain"])
    gv = re.sub("[0-9]+:l", ":l", gv)
    gv = re.sub("[0-9]+:r", ":r", gv)
    gv = re.sub("[0-9]+:v", ":v", gv)
    gv = re.sub(r"[0-9]+ -> [0-9]+ \[label=", " ->  [label=", gv)
    gv = re.sub(r"[0-9]+ \[label=", " [label=", gv)
    assert same_content(selenium_py3old, "python3-old_binarytree.gv", gv)


def test_audioop(selenium_py3old):
    result = selenium_py3old.run_basthon("import wave")
    assert result["stdout"] == ""
    assert result["stderr"] == ""
