PY?=python3
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

GALERIE=$(BASEDIR)/theme/static/assets/img/galerie/
GALERIE_IMAGES=$(wildcard $(GALERIE)/*/*.png)
GALERIE_THUMBNAILS=$(shell for f in $(GALERIE_IMAGES); do echo $(GALERIE)/.thumbnails/$$(basename $$(dirname $$f))/$$(basename $$f); done)

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

help:
	@echo 'Makefile for a pelican Web site                                           '
	@echo '                                                                          '
	@echo 'Usage:                                                                    '
	@echo '   make html                           (re)generate the web site          '
	@echo '   make clean                          remove the generated files         '
	@echo '   make regenerate                     regenerate files upon modification '
	@echo '   make publish                        generate using production settings '
	@echo '   make serve [PORT=8000]              serve site at http://localhost:8000'
	@echo '   make serve-global [SERVER=0.0.0.0]  serve (as root) to $(SERVER):80    '
	@echo '   make devserver [PORT=8000]          serve and regenerate together      '
	@echo '                                                                          '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html   '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                    '
	@echo '                                                                          '
BASTHON_DOC=$(BASEDIR)/theme/static/assets/pdf/Basthon_Documentation.pdf

$(BASTHON_DOC): $(BASEDIR)/../doc/doc.tex $(BASEDIR)/../doc/documentation.cls
	mkdir -p $$(dirname $@)
	cd $$(dirname $<) && make $$(basename $< .tex).pdf && cd -
	mv $$(dirname $<)/$$(basename $< .tex).pdf $@

sphinx-doc:
	cd ../sphinx-doc/ && make html && cd -
	mkdir -p $(OUTPUTDIR)
	rm -rf $(OUTPUTDIR)/doc
	cp -r ../sphinx-doc/build/html/ $(OUTPUTDIR)/doc/


doc: $(BASTHON_DOC) sphinx-doc

thumbnails: $(GALERIE_THUMBNAILS)

$(GALERIE)/.thumbnails/%.png: $(GALERIE)/%.png
	mkdir -p $(basename $@)
	convert $< -colorspace RGB -resize 256x144^ -gravity center -extent 256x144 -set colorspace RGB $@

html: thumbnails doc galerie.py
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR) ; rm -f $(BASTHON_DOC)

regenerate:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
ifdef PORT
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

serve-global:
ifdef SERVER
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b $(SERVER)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b 0.0.0.0
endif


devserver:
ifdef PORT
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

publish:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

.PHONY: html help clean regenerate serve serve-global devserver stopserver publish thumbnails doc sphinx-doc
