import { KernelMainBase } from "@basthon/kernel-base/worker";
import { JavaScriptKernelWorker } from "./worker";

export class JavaScriptKernel extends KernelMainBase<JavaScriptKernelWorker> {
  constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      type: "module",
      //@ts-ignore
      sandboxed: true,
    });
  }

  public language() {
    return "javascript";
  }
  public languageName() {
    return "Javascript";
  }
  public moduleExts() {
    return ["js"];
  }

  public ps1() {
    return " js> ";
  }

  public ps2() {
    return "...> ";
  }

  public more(source: string) {
    return false;
  }

  public async complete(code: string): Promise<[string[], number] | []> {
    return (await this.remote?.complete(code)) ?? [];
  }

  public async putFile(filename: string, content: ArrayBuffer) {
    console.error(
      `Fichier ${filename} not added since putFile has no mean in the JS context.`
    );
  }

  public async putModule(filename: string, content: ArrayBuffer) {
    content = new Uint8Array(content);
    const ext = filename.split(".").pop();
    switch (ext) {
      case "js":
        let decoder = new TextDecoder("utf-8");
        const _content = decoder.decode(content);
        await this.remote?.eval({}, _content);
        break;
      default:
        throw { message: "Only '.js' files supported." };
    }
  }
}
