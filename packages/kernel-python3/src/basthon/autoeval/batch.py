from contextlib import contextmanager
from typing import Iterable, Callable, Sequence, Tuple
import sys
from .validate import validationclass, Validate

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = ["ValidateAll", "ValidateAny"]


@contextmanager
def catch_streams(stdout_prefix: str, stderr_prefix: str):
    """Catch ``stdout`` and ``stderr`` streams and redirect them
    for the grouped validation process.
    Print prefixes before each call to write on each stream."""
    streams = {"stdout": {"prefix": stdout_prefix}, "stderr": {"prefix": stderr_prefix}}
    last_writer = None
    for name, data in streams.items():
        stream = getattr(sys, name)
        data["backup"] = getattr(stream, "write")

        def write(text: str) -> None:
            nonlocal last_writer
            if last_writer != name:
                text = f"{data['prefix']}\n{text}"
                last_writer = name
            text = text.replace("\n", "\n\t")
            data["backup"](text)

        setattr(stream, "write", write)
    yield
    # restoration
    for name, data in streams.items():
        stream = getattr(sys, name)
        setattr(stream, "write", data["backup"])


@validationclass
class ValidateBatch:
    """Validate a list of objects, instances of the :class:`Validate` class.
    The way we decide to validate the whole batch is given by the
    ``success_tester`` parameter.
    """

    def __init__(
        self,
        validators: Iterable[Validate],
        success_tester: Callable[[Sequence[bool]], Tuple[bool, bool]],
        keep_going: bool = False,
    ):
        """
        :param validators: the list of validators
        :param success_tester: a function that given the current list of
            boolean successes, returns two booleans (in a tuple): the
            first tells whether the final result is already known (no need
            to compute the remaining) and the second gives the current result
        :param keep_going: indicate if we keep going to call validators, no
            matter if the final result is already known.
        """
        validators = list(validators)
        for validator in validators:
            if not isinstance(validator, Validate):
                raise ValueError(
                    f"The object '{validator}' is not an instance of Validate."
                )
            validator.ignore_breakpoint(True)
        self._validators: list[Validate] = validators
        self._success_tester = success_tester
        self._keep_going = keep_going

    def __call__(self):
        """Call all the validators in the order given in constructor."""
        successes = []
        for index, validator in enumerate(self._validators):
            # capture streams and call validation
            out_prefix = f"La validation n°{index+1} affiche :"
            err_prefix = f"La validation n°{index+1} affiche en erreur :"
            with catch_streams(out_prefix, err_prefix):
                successes.append(validator())
            # analyse success
            if not self._keep_going:
                return_now, success = self._success_tester(successes)
                if return_now:
                    return success
        return self._success_tester(successes)[1]


@validationclass
class ValidateAll(ValidateBatch):
    """Validate a list of objects, instances of the :class:`Validate` class.
    The validation succeeded when all validation succeeded."""

    def __init__(self, *args, **kwargs):
        kwargs["success_tester"] = lambda s: (False, True) if all(s) else (True, False)
        super().__init__(*args, **kwargs)


@validationclass
class ValidateAny(ValidateBatch):
    """Validate a list of objects, instances of the :class:`Validate` class.
    The validation succeeded when one validation succeeded."""

    def __init__(self, *args, **kwargs):
        kwargs["success_tester"] = lambda s: 2 * (any(s),)
        super().__init__(*args, **kwargs)
