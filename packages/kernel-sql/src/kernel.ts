import { KernelMainBase } from "@basthon/kernel-base/worker";
import { SQLKernelWorker } from "./worker";

export class SQLKernel extends KernelMainBase<SQLKernelWorker> {
  private _dbInit: any = undefined;
  private _sqlsInit: string[] = [];

  public constructor(options: any) {
    super(options);
  }

  protected newWorker(): Worker {
    return new Worker(new URL("./comlink-worker.js", import.meta.url), {
      type: "module",
      //@ts-ignore
      sandboxed: true,
    });
  }

  language() {
    return "sql";
  }
  languageName() {
    return "SQL";
  }
  moduleExts() {
    return ["sql", "db", "sqlite"];
  }

  public ps1() {
    return "sql> ";
  }

  public ps2() {
    return "...> ";
  }

  public async stop(): Promise<void> {
    await this.remote?.closeDB();
    await super.stop();
  }

  public async start(options?: any): Promise<void> {
    if (options == null) options = { db: this._dbInit, sqls: this._sqlsInit };
    await super.start(options);
  }

  public more(source: string) {
    return false;
  }

  public async complete(code: string): Promise<[string[], number] | []> {
    return [];
  }

  public async putFile(filename: string, content: ArrayBuffer) {
    // just ignoring this since we don't have a filesystem.
  }

  public async putModule(filename: string, content: ArrayBuffer) {
    content = new Uint8Array(content);
    const ext = filename.split(".").pop();
    switch (ext) {
      case "sql":
        let decoder = new TextDecoder("utf-8");
        const _content = decoder.decode(content);
        await this.remote?.runCode(_content);
        // SQL strings are concatenated
        this._sqlsInit.push(_content);
        break;
      case "sqlite":
      case "db":
        // initial db file is replaced
        this._dbInit = content;
        await this.restart();
        break;
      default:
        throw { message: "Only '.sql', '.db' and '.sqlite' files supported." };
    }
  }
}
